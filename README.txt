----------------------------------------------
Views Auto Fields
 by Ted Bowman(tedbow) at Six Mile Tech
----------------------------------------------

This module provides a framework for adding fields dynamically to Views.

Modules must extend views_auto_fields_handler_field_callback

Then the field that extend views_auto_fields_handler_field_callback should be
added to a View.
