<?php

/**
 * @file
 * Views integration for the views_auto_fields module.
 */

/**
 * Implements hook_views_data_alter().
 *
 * Provide the dynamic field handler.
 * Even though views_auto_fields_handler_field_callback is field handler class
 * it should not be defined here because it is an abstract class that another
 * class needs to extend.
 */
function views_auto_fields_views_data_alter(&$data) {
  $data['views']['views_auto_fields_callback_dynamic']['field'] = array(
    'title' => t('Callback Dynamic Field'),
    'help' => t('donnot add this.'),
    'handler' => 'views_auto_fields_handler_field_callback_dynamic',
  );
}

/**
 * Implements hook_views_pre_view().
 *
 * We need to use this hook to dynamically add the fields.
 * This is possible only using the field handler class.
 */
function views_auto_fields_views_pre_view(view &$view, &$display_id, &$args) {
  $view_field = _views_auto_fields_get_views_auto_fields_field($view, $display_id);
  if (!$view_field) {
    $view_field = _views_auto_fields_get_views_auto_fields_field($view, 'default');
  }
  if ($view_field) {
    // add dynamic fields
    if (is_object($view_field)) {
      $dynamic_fields = $view_field->field_callback($args);
    }
    else {
      /* @var views_auto_fields_handler_field_callback $handler */
      $handler = new $view_field['class_name']();
      $dynamic_fields = $handler->field_callback($args);
    }

    foreach ($dynamic_fields as $dynamic_field) {
      $view->add_item($display_id, 'field', "views", "views_auto_fields_callback_dynamic", $dynamic_field);
    }
  }
}

/**
 * Get the Field by this module.
 *
 * @param view $view
 * @param string $display_id
 *
 * @return views_auto_fields_handler_field_callback
 */
function _views_auto_fields_get_views_auto_fields_field($view, $display_id = NULL) {
  if (isset($view->display_handler->handlers['field'])) {
    $fields = $view->display_handler->handlers['field'];
  }
  else {
    if ($display_id) {
      $display = $view->display[$display_id];
    }
    else {
      $display = $view->display[$view->current_display];
    }
    if ((isset($display->display_options['row_plugin']) && $display->display_options['row_plugin'] == 'fields') || $display->handler->options['row_plugin'] == 'fields') {
      if (isset($display->display_options['fields'])) {
        $fields = $display->display_options['fields'];
      }
      else {
        // This display doesn't have field specified
        $fields = $view->display['default']->display_options['fields'];
      }
    }
  }
  if (isset($fields)) {
    foreach ($fields as $field) {
      if (is_object($field)) {
        if (is_subclass_of($field, 'views_auto_fields_handler_field_callback')) {
          return $field;
        }
      }
      else {
        // Unfortunately early the View build we will not have the Field as an object.
        if (!empty($field['_is_views_auto_field_callback'])) {
          return $field;
        }
      }

    }
  }
  return NULL;
}
