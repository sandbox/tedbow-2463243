<?php
/**
 * Handler for field added dynamically.
 * @file views_auto_fields_handler_field_callback_dynamic class.
 */

class views_auto_fields_handler_field_callback_dynamic extends views_handler_field_custom {
  function render($values) {
    $view_auto_field = _views_auto_fields_get_views_auto_fields_field($this->view);
    $index = $this->options['views_auto_field_index'];
    return $view_auto_field->row_values[$index];
  }

}
